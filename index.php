<?php
include_once "config.php";
$url = explode("/", $_SERVER["REQUEST_URI"]);
$controller = $url[2];
if (strpos($controller, "?") !== false)
	$controller = explode("?", $controller)[0];
include "controllers/$controller.php";
$obj = new $controller($dbConfig);
switch ($_SERVER['REQUEST_METHOD']):
	case 'GET':
		$_POST = $_GET;
		$method = "one";
		if (isset($url[3]))
			$method = $url[3];
		$answ = $obj->$method();
		$obj->conn->close();
		header("Content-type: application/json", true, $answ['header']['status']);
		echo json_encode($answ['body']);
	break;
	case "POST":
		$_POST = json_decode(file_get_contents("php://input"), true);
		$method = "add";
		if (isset($url[3])) 
			$method = $url[3];
		$answ = $obj->$method();
		$obj->conn->close();
		header("Content-type: application/json", true, $answ['header']['status']);
		echo json_encode($answ['body']);
	break;
	case "PUT":
		$_POST = json_decode(file_get_contents("php://input"), true);
		$method = "edit";
		if (isset($url[3])) 
			$method = $url[3];
		$answ = $obj->$method();
		$obj->conn->close();
		header("Content-type: application/json", true, $answ['header']['status']);
		echo json_encode($answ['body']);
	break;
	case "DELETE":
	break;
	default:
		$method = $url[3];
		$obj->$method();
		$obj->conn->close();
	break;
endswitch;