<?php
require './core/controller.php';
class Alumnos extends controller {

	public $fields = array(
		"nombre", "paterno", "materno", "natalicio", "sexo",
		"id_usuario", "id_carrera"
	);
	public $table = "alumnos";
	public $objName = "alumno";

	public function __construct ($dbConfig) {
		parent::__construct($dbConfig);
	}

	public function add () {
		$this->rules->add("nombre", "Nombre", "required");
		$this->rules->add("paterno", "Paterno", "required");
		$this->rules->add("materno", "Materno", "required");
		$this->rules->add("natalicio", "Fecha de Nacimiento", "required|callback[validarNatalicio]");
		$this->rules->add("sexo", "Sexo", "required|callback[validarSexo]");
		$this->rules->add("id_usuario", "Usuario", "required|fk[usuarios.id]|unique[usuarios.id]");
		$this->rules->add("id_carrera", "Carrera", "required|fk[carreras.id]");
		return parent::insert();
	}

	public function edit () {
		$this->rules->add("id", "ID", "required|fk[alumnos.id]");
		$this->rules->add("natalicio", "Fecha de Nacimiento", "callback[validarFecha]");
		$this->rules->add("sexo", "Sexo", "callback[validarSexo]");
		$this->rules->add("id_usuario", "Usuario", "fk[usuarios.id]|unique[usuarios.id]");
		$this->rules->add("id_carrera", "Carrera", "fk[carreras.id]");
		if ($this->rules->validate()):
			$where = " WHERE id = '" . $_POST['id'] . "'";
			$this->answ = parent::update($where);
		else:
			$this->answ['body']['error'] = $this->rules->errors();
		endif;
		return $this->answ;
	}

	public function one () {
		$this->rules->add("id", "ID", "require|fk[alumnos.id]");
		if ($this->rules->validate()) {
			$where = " WHERE id = {$_POST['id']} AND status = 1";
			$this->answ = parent::get($where);
		} else {
			$this->setError($this->rules->errors());
		}
		return $this->answ;
	}
}