<?php
require './core/controller.php';
class Contactos extends controller {

	public $fields = array(
		"id_alumno", "telefono", "celular", "email", "email_alt"
	);
	public $table = "contactos";
	public $objName = "contacto";

	public function add () {
		$this->rules->add("id_alumno", "Alumno", "requied|fk[alumnos.id]");
		$this->rules->add("email", "Correo", "required|unique[contactos.email]|callback[validarCorreo]");
		$this->rules->add("email_alt", "Correo Alterno", "unique[contactos.email_alt]|callback[validarCorreo]");
		$this->rules->add("telefono", "Teléfono", "unique[contactos.telefono]|callback[validarNumero]");
		$this->rules->add("celular", "Celular", "unique[contactos.celular]|callback[validarNumero]");
		return parent::insert();
	}

	public function edit () {
		$this->rules->add("id", "ID", "required|fk[contactos.id]");
		$this->rules->add("email", "Correo", "unique[contactos.email]|callback[validarCorreo]");
		$this->rules->add("email_alt", "Correo Alterno", "unique[contactos.email_alt]|callback[validarCorreo]");
		$this->rules->add("telefono", "Teléfono", "unique[contactos.telefono]|callback[validarNumero]");
		$this->rules->add("celular", "Celular", "unique[contactos.celular]|callback[validarNumero]");
		if ($this->rules->validate()):
			$where = " WHERE id = '" . $_POST['id'] . "'";
			$this->answ = parent::update($where);
		else:
			$this->answ['body']['error'] = $this->rules->errors();
		endif;
		return $this->answ;
	}


	public function one () {
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		if ($this->rules->validate()):
			$where = " WHERE id_alumno = '{$_POST['id_alumno']}' AND status = 1";
			$this->answ = parent::get($where);
		else:
			$this->setError($this->rules->errors());
		endif;
		return $this->answ;
	}
}
