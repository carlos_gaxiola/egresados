<?php
require './core/controller.php';
class Trabajos extends controller {

	public $fields = array(
		"nombre", "telefono", "area", "descripcion", "sueldo_mensual",
		"inicio", "fin", "num_calle", "colonia", "id_alumno"
	);
	public $table = "trabajos";
	public $objName = "trabajo";

	public function __construct ($dbConfig) {
		parent::__construct($dbConfig);
	}

	public function add () {
		$this->conn->query("BEGIN");
		$this->rules->add("nombre", "Nombre", "required");
		$this->rules->add("telefono", "Teléfono", "required|callback[validarNumero]|unique[trabajos.telefono]");
		$this->rules->add("area", "Área", "required");
		$this->rules->add("descripcion", "Descripción", "required");
		$this->rules->add("inicio", "Inicio", "required|callback[validarFecha]");
		$this->rules->add("fin", "Fin", "required|callback[validarFecha]|callback[valFechaFin]");
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		parent::insert();
		if ($this->getStatus() == 400):
			$this->conn->query("COMMIT");
		elseif ($this->getStatus() == 200 && $this->delLastWork()):
			$this->conn->query("COMMIT");
		else:
			$this->conn->query("ROLLBACK");
			$this->setStatus(500);
			$this->setObj(false);
		endif;
		return $this->answ;
	}

	public function edit () {
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		$this->rules->add("telefono", "Teléfono", "callback[validarNumero]");
		$this->rules->add("inicio", "Inicio", "callback[validarFecha]");
		$this->rules->add("fin", "Fin", "callback[validarFecha]|callback[valFechaFin]");
		$this->rules->add("id_alumno", "Alumno", "fk[alumnos.id]");
		if ($this->rules->validate()):
			$where = " WHERE id_alumno = '" . $_POST['id'] . "' AND status = 1;";
			parent::update($where);
		else:
			$this->setError($this->rules->errors());
		endif;
		return $this->answ;
	}

	public function one () {
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		if ($this->rules->validate()):
			$where = " WHERE id_alumno = '{$_POST['id_alumno']}' AND status = 1";
			$this->answ = parent::get($where);
		else:
			$this->setError($this->rules->errors());
		endif;
		return $this->answ;
	}

	public function delLastWork () {
		$currentWork = $this->getObj();
		$del = "UPDATE $this->table SET status = 0 " . 
			"WHERE id_alumno = '".$currentWork["id_alumno"].
			"' AND id != '".$currentWork["id"]."'";
		$this->conn->query($del);
		if ($this->conn->error) {
			$this->answ['body']['mysql_error'] = $this->conn->error;
			return false;
		}
		return true;
	}

	public function valFechaFin ($fecha) {
		$valid = isset($_POST['inicio']) && isset($_POST['fin']);
		if ($valid) {
			$inicio = DateTime::createFromFormat("Y-m-d", $_POST["inicio"]);
			$fin = DateTime::createFromFormat("Y-m-d", $_POST["fin"]);
			$valid = $inicio <= $fin;
		}
		if (!$valid)
			$this->rules->message("valFechaFin", "La fecha fin debe ser mayor a fecha inicio");
		return $valid;
	}
}