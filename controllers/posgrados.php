<?php
require './core/controller.php';
class Posgrados extends controller {

	public $fields = array(
		"nombre", "deseo", "id_alumno"
	);
	public $table = "posgrados";
	public $objName = "posgrado";

	public function __construct ($dbConfig) {
		parent::__construct($dbConfig);
	}

	public function add () {
		$this->rules->add("nombre", "Nombre", "required");
		$this->rules->add("deseo", "Deseo", "required");
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		parent::insert();
		if ($this->getStatus() == 400):
			$this->conn->query("COMMIT");
		elseif ($this->getStatus() == 200 && $this->delLastPostgrade()):
			$this->conn->query("COMMIT");
		else:
			$this->conn->query("ROLLBACK");
			$this->setStatus(500);
			$this->setObj(false);
		endif;
		return $this->answ;
	}

	public function edit () {
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		if ($this->rules->validate()):
			$where = " WHERE id_alumno = '" . $_POST['id_alumno'] . "'" .
				" AND status = 1";
			parent::update($where);
		else:
			$this->setError($this->rules->error());
		endif;
		return $this->answ;
	}

	public function one () {
		$this->rules->add("id_alumno", "Alumno", "required|fk[alumnos.id]");
		if ($this->rules->validate()):
			$where = " WHERE id_alumno = '{$_POST['id_alumno']}' AND status = 1";
			$this->answ = parent::get($where);
		else:
			$this->setError($this->rules->errors());
		endif;
		return $this->answ;
	}

	public function delLastPostgrade () {
		$curPost = $this->getObj();
		$del = "UPDATE $this->table SET status = 0 " . 
			"WHERE id_alumno = '".$curPost["id_alumno"].
			"' AND id != '".$curPost["id"]."'";
		$this->conn->query($del);
		if ($this->conn->error) {
			$this->answ['body']['mysql_error'] = $this->conn->error;
			return false;
		}
		return true;
	}
}