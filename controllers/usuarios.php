<?php
require './core/controller.php';
class Usuarios extends controller {

	public $fields = array(
		"usuario", "password", "id_perfil", "email", "url_foto"
	);
	public $table = "usuarios";
	public $objName = "usuario";

	public function __construct ($dbConfig) {
		parent::__construct($dbConfig);
	}

	public function add () {
		$this->rules->add("usuario", "Usuario", "required|unique[usuarios.usuario]");
		$this->rules->add("password", "Contraseña", "required");
		$this->rules->add("id_perfil", "Perfil", "required|fk[perfiles.id]");
		return parent::insert();
	}

	public function edit () {
		$this->rules->add("id", "ID", "required|fk[usuarios.id]");
		$this->rules->add("usuario", "Usuario", "unique[usuarios.usuario]");
		$this->rules->add("id_perfil", "Perfil", "fk[perfiles.id]");
		if ($this->rules->validate()):
			$where = " WHERE id = '" . $_POST['id'] . "'";
			$this->answ = parent::update($where);
		else:
			$this->answ['body']['error'] = $this->rules->errors();
		endif;
		return $this->answ;
	}

	public function one () {
		$this->rules->add("id", "ID", "require|fk[alumnos.id]");
		if ($this->rules->validate()) {
			$where = " WHERE id = {$_POST['id']} AND status = 1";
			$this->answ = parent::get($where);
		} else {
			$this->setError($this->rules->errors());
		}
		return $this->answ;
	}

	public function login () {
		$this->rules->add("usuario", "Usuario", "required");
		$this->rules->add("password", "Contraseña", "required");
		if ($this->rules->validate()) {
			$login = "SELECT * FROM usuarios WHERE usuario = '" 
				. $_POST['usuario'] . "' AND password = '" 
				. $_POST['password'] . "'";
			$user = $this->conn->query($login);
			if ($user->num_rows > 0) {
				$answ['body']['user'] = $user->fetch_assoc();
				$answ['header']['status'] = 200;
			} else {
				$answ['body']['user'] = false;
				$answ['header']['status'] = 210;
			}
		} else {
			$answ['header']['status'] = 400;
			$answ['body']['error'] = $this->rules->errors();
		}
		return $answ;
	}
}