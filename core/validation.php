<?php
class Rules {

	public $fields = array();
	public $rulesMessages = array();
	public $errors = array();
	public $conn; 

	public function __construct ($conn, $callbackOwner) {
		$this->conn = $conn;
		$this->callbackOwner = $callbackOwner;
	}

	public function add ($field, $label, $rules) {
		$this->fields[$field] = array(
			"label" => $label, "rules" => $rules
		);
	}

	public function message ($rule, $message) {
		$this->rulesMessages[$rule] = $message;
	}

	public function validate () {
		$ok = true;
		foreach ($this->fields as $field => $superValue):
			$rules = explode("|", $superValue['rules']);
			$msg = "";
			foreach ($rules as $key => $value):
				$valid = isset($_POST[$field]) && !empty($_POST[$field]);
				if (strcmp("required", $value) == 0):
					if (!$valid):
						$msg = "El campo " . $superValue['label'] . " es obligatorio.";
						if (isset($this->rulesMessages["required"])):
							$msg = str_replace("%s", $superValue['label'], $this->rulesMessages['required']);
						endif;
						$ok = false;
					endif;
				elseif ($valid && strpos($value, "callback") !== false):
					$callback = substr($value, 9, -1);
					$valid = $this->callbackOwner->$callback($_POST[$field]);
					if (!$valid):
						$msg = str_replace("%s", $superValue['label'], $this->rulesMessages[$callback]);
						$ok = false;
					endif;
				elseif ($valid && strpos($value, "unique") !== false):
					$creteria = explode(".", substr($value, 7, -1));
					$verifier = "SELECT * FROM " . $creteria[0] . 
						" WHERE " . $creteria[1] . " = '" . $_POST[$field] . "'";
					$result = $this->conn->query($verifier);
					$valid = $result->num_rows == 0 || $result->num_rows == 1;
					if (!$valid):
						$msg = "El valor para " . $superValue['label'] . " ya existe.";
						if (isset($this->rulesMessages['required'])):
							$msg = str_replace("%s", $superValue['label'], $this->rulesMessages['unique']);
						endif;
						$ok = false;
					endif;
				elseif ($valid && strpos($value, "fk") !== false):
					$creteria = explode(".", substr($value, 3, -1));
					$verifier = "SELECT * FROM " . $creteria[0] . 
						" WHERE " . $creteria[1] . " = '" . $_POST[$field] . "'";
					$result = $this->conn->query($verifier);
					$valid = $result->num_rows > 0;
					$result->free();
					if (!$valid):
						$msg = "El valor para " . $superValue['label'] . " no es válido.";
						if (isset($this->rulesMessages['required'])):
							$msg = str_replace("%s", $superValue['label'], $this->rulesMessages['fk']);
						endif;
						$ok = false;
					endif;
				endif;
			endforeach;
			if ($msg !== '')
				$this->errors[$field] = $msg;
		endforeach;
		return $ok;
	}

	public function errors () {
		return $this->errors;
	}
}