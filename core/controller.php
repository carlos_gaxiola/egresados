<?php
require './config.php';
require "validation.php";
class controller {

	public $errors = array();
	public $conn;
	public $fields;
	public $table;
	public $answ = array(
		"header" => array(
			"status" => 400
		),
		"body" => array()
	);
	public $rules;

	public function __construct ($dbConfig) {
		$this->conn = new mysqli($dbConfig['hostname'], $dbConfig['user'], $dbConfig['password'], $dbConfig['database'])
			or die ("No se ha podido conectar al servidor de Base de datos");
		$this->rules = new Rules($this->conn, $this);
	}

	public function insert () {
		if ($this->rules->validate()):
			$insert = "INSERT INTO $this->table SET ";
			foreach ($this->fields as $field):
				$val = isset($_POST[$field]) ? $_POST[$field] : null;
				$insert .= $field . " = '" . $val . "', ";
			endforeach;
			$insert = trim($insert, ", ");
			$this->conn->query($insert);
			if ($this->conn->affected_rows > 0):
				$select = "SELECT * FROM $this->table WHERE id = ". $this->conn->insert_id;
				$result = $this->conn->query($select);
				if ($result->num_rows > 0):
					$this->setObj($result->fetch_assoc());
					$this->setStatus(200);
				else:
					$this->setStatus(500);
					$this->setError("No se pudo recuperar el registro.");
				endif;
			else:
				$this->setStatus(500);
				$this->setError("No se pudo insertar.");
			endif;
		else:
			$this->setError($this->rules->errors());
		endif;
		return $this->answ;
	}

	public function update ($where = null) {
		$update = "UPDATE $this->table SET ";
		foreach ($this->fields as $field):
			if (isset($_POST[$field]))
				$update .= $field . " = '" . $_POST[$field] . "', ";
		endforeach;
		$update = trim($update, ", ") . $where;
		$this->conn->query($update);
		if ($this->conn->affected_rows > 0):
			$select = "SELECT * FROM $this->table " . $where;
			$result = $this->conn->query($select);
			if ($result->num_rows > 0):
				$this->setObj($result->fetch_assoc());
				$this->setStatus(200);
			else:
				$this->setStatus(500);
				$this->setError("No se pudo recuperar el registro.");
			endif;
		else:
			$this->setStatus(500);
			$this->setError("No se pudo actualizar.");
		endif;
		return $this->answ;
	}

	public function get ($where = null) {
		if ($where == null):
			$where = " LIMIT 1";
			if (isset($_POST['id'])):
				$where = " id = '" . $_POST['id'] . "'";
			endif;
		endif;
		$select = "SELECT * FROM $this->table " . $where;
		$result = $this->conn->query($select);
		if ($result->num_rows > 0):
			$this->setObj($result->fetch_assoc());
			$this->setStatus(200);
		else:
			$this->setStatus(500);
			$this->setError("No se pudo recuperar el registro.");
		endif;
		return $this->answ;
	}

	public function validarNumero ($numero) {
		$valid = is_numeric($numero) && strlen($numero) == 10;
		if (!$valid)
			$this->rules->message("validarNumero", "El %s ingresado no es válido.");
		return $valid;
	}

	public function validarCorreo ($correo) {
		$valid = strpos($correo, "@");
		if (!$valid)
			$this->rules->message("validarCorreo", "El %s ingresado no es válido.");
		return $valid;
	}

	public function validarSexo ($sexo) {
		$valido = $sexo == "H" || $sexo == "M";
		if (!$valido)
			$this->rules->message("validarSexo", "El sexo ingresado no es válido.");
		return $valido;
	}

	public function validarFecha ($fecha) {
		$fecha = explode("-", $fecha);
		$valido = checkdate($fecha[1], $fecha[2], $fecha[0]);
		if (!$valido)
			$this->rules->message("validarFecha", "La %s ingresada no es válida.");
		return $valido;
	}

	public function setStatus 	($status) 	{ $this->answ['header']['status'] = $status; 	}
	public function setError 	($error) 	{ $this->answ['body']['error'] = $error; 		}
	public function setObj 		($obj) 		{ $this->answ['body'][$this->objName] = $obj; 	}

	public function getStatus 	() { return $this->answ['header']['status']; 	}
	public function getError 	() { return $this->answ['body']['error']; 		}
	public function getObj 		() { return $this->answ['body'][$this->objName];}
}